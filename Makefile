ifneq (${KERNELRELEASE},)
	obj-m  = rpi_rf_ook.o
else
	KERNEL_DIR ?= /lib/modules/$(shell uname -r)/build
	MODULE_DIR := $(shell pwd)

.PHONY: all

all: modules

.PHONY:modules

modules:
	${MAKE} -C ${KERNEL_DIR} M=${MODULE_DIR}  modules

modules_install:
	${MAKE} -C ${KERNEL_DIR} M=${MODULE_DIR}  modules_install

clean:
	${MAKE} -C ${KERNEL_DIR} M=${MODULE_DIR}  clean
endif
