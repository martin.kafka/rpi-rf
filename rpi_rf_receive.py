#!/usr/bin/python

# Nexus sensor protocol with ID, temperature and optional humidity
# also FreeTec NC-7345 sensors for FreeTec Weatherstation NC-7344.
#
# the sensor sends 36 bits 12 times,
# the packets are ppm modulated (distance coding) with a pulse of ~500 us
# followed by a short gap of ~1000 us for a 0 bit or a long ~2000 us gap for a
# 1 bit, the sync gap is ~4000 us.
#
# the data is grouped in 9 nibbles
# [id0] [id1] [flags] [temp0] [temp1] [temp2] [const] [humi0] [humi1]
#
# The 8-bit id changes when the battery is changed in the sensor.
# flags are 4 bits B 0 C C, where B is the battery status: 1=OK, 0=LOW
# and CC is the channel: 0=CH1, 1=CH2, 2=CH3
# temp is 12 bit signed scaled by 10
# const is always 1111 (0x0F)
# humiditiy is 8 bits
#
# The sensor can be bought at Clas Ohlsen

import collections
import datetime
import time
import sys
import traceback

DEVICE='/dev/rpirfook'

devf = None
buff = collections.deque(maxlen = 2048)
last_timestamp = None

def read_dev():
    global devf
    lines_read = 0
    #print >> sys.stderr, ">>>" + str(datetime.datetime.now())
    while True:
        line = devf.readline()
        if line == "":
            break
        lines_read = lines_read + 1
        a = line.split(' ')
        v = int(a[0])
        d = int(a[1])
        #print >> sys.stderr, line.rstrip()
        buff.append([v, d])
    #print "lines read: " + str(lines_read)

def nible(b):
    return (b[0] << 3) | (b[1] << 2) | (b[2] << 1) | b[3]

def byte(b):
    return (nible(b[0:4]) << 4) | nible(b[4:8])

def parse_packet(b):
    id = byte(b[0:8])
    flags = nible(b[8:12])
    temp = (byte(b[12:20]) << 4) | nible(b[20:24])
    if temp & (1 << 11):
        temp = -(~temp & 0xFFF) - 1
    const = nible(b[24:28])
    humi = byte(b[28:36])

    bat = b[8]
    ch = flags & 0x3

    if const != 15: # invalid packet
        return None

    return { 'id': id, 'flags': flags, 'battery': bat, 'channel': ch, 'temp': temp/10.0, 'humidity': humi }

def extract_packet(bitrows):
    if len(bitrows) < 3:
        return None

    rows = []
    for b in bitrows:
        if len(b) >= 36 and len(b) <= 37:
            rows.append(b)

    maxidx = None
    maxcount = 0
    for i in range(len(rows)):
        count = 0
        for j in range(len(rows)):
            if i != j and rows[i] == rows[j]:
                count += 1
        if maxcount < count:
            maxidx = i
            maxcount = count

    if maxcount < 3:
        return None
    else:
        print "maxcount: " + str(maxcount)
        return rows[maxidx]

try:
    print "Start " + str(datetime.datetime.now())
    with open(DEVICE, 'r') as devf:
        bitrows = []
        bits = []
        while True:
            read_dev()
            while True:
                item = None
                if buff:
                    item = buff.popleft()
                if item is None:
                    break

                #print "item: " + str(item[0]) + ":" + str(item[1])
                endPacket = False
                if item[0] == 1:
                    if item[1] < 300 or item[1] > 700:
                        #endPacket = True
                        None
                else:
                    gap = item[1]
                    if gap > 4500:
                        endPacket = True
                    elif gap < 1500:
                        bits.append(0)
                    elif gap < 2500:
                        bits.append(1)
                    else:
                        if len(bits) > 0:
                            bitrows.append(bits)
                        bits = []

                    if endPacket:
                        if len(bits) > 0:
                            bitrows.append(bits)
                        bits = []
                        packet = extract_packet(bitrows)
                        if packet is not None:
                            strdata = "".join([str(b) for b in packet]) + ":" + str(len(packet))
                            print "Packet (" + str(datetime.datetime.now()) + "): " + strdata
                            data = parse_packet(packet)
                            strdata = 'id: {0}, ch: {1}, bat: {2}, temp: {3:0.3f}, humi: {4:0.2f}'.format(data['id'], data['channel'], data['battery'], data['temp'], data['humidity'])
                            print "  " + strdata
                        bitrows = []
            time.sleep(0.5)
except Exception,e:
    print "Error: " + str(e)
    traceback.print_exc()

