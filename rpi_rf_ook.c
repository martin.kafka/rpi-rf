/*
 * Basic Linux Kernel module using GPIO interrupts.
 *
 * Author:
 * 	Interrupt handling part - Stefan Wendler (devnull@kaltpost.de)
 *  Device part :
 *     Copyright (C) 2013, Jack Whitham
 *     Copyright (C) 2009-2010, University of York
 *     Copyright (C) 2004-2006, Advanced Micro Devices, Inc.
 *
 *  Modified by Disk91 (www.disk91.com) for RPIRF Shield
 *  And then all changed by Mc
 *
 * This software is licensed under the terms of the GNU General Public
 * License version 2, as published by the Free Software Foundation, and
 * may be copied, distributed, and modified under those terms.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
#include <linux/device.h>
#include <linux/miscdevice.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/time.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/spinlock.h>
#include <linux/circ_buf.h>

#define DEV_NAME 			"rpirfook" 
#define IRQ_ENTRIES_NAME	"rpirfook#rx"
#define BUFFER_SZ			2048 

static char gpio_pin = 0;
module_param(gpio_pin, byte, S_IRUGO);

static long event_buff[BUFFER_SZ];
static int pTail;
static int pHead;
//static spinlock_t spinlock;
static int wasOverflow;

static struct timespec interval_start;
static struct timespec interval_end;
static int interval_value;

/* Later on, the assigned IRQ numbers for the pin is stored here */
static int rx_irq = -1;

/*
 * The interrupt service routine called on every pin status change
 */
static irqreturn_t rx_isr(int irq, void *data)
{
   	struct timespec current_time;
    struct timespec delta_time;
	long delta;
	int value;
	int head;
	int tail;
	
	//spin_lock_init(&spinlock);

   	getnstimeofday(&current_time);

	value = gpio_get_value(gpio_pin);
	delta_time = timespec_sub(current_time, interval_end);
	delta = (long)(((long long)delta_time.tv_sec * 1000000) + (delta_time.tv_nsec / 1000)); 

	if (delta > 100) {
		if (value == interval_value) {
			// if the same value, just move end and wait for next
			interval_end = current_time;
		} else {
			// flush data and setup new
			delta_time = timespec_sub(interval_end, interval_start);
			delta = (long)(((long long)delta_time.tv_sec * 1000000) + (delta_time.tv_nsec / 1000)); 

			if (delta < 0) delta = LONG_MAX;
			if (interval_value) delta = -delta;

			head = pHead;
			tail = ACCESS_ONCE(pTail);
			if (CIRC_SPACE(head, tail, BUFFER_SZ) > 0) {
				// insert one item into the event_buffer 
				event_buff[head] = delta;
				smp_wmb();
				smp_store_release(&pHead, (head + 1) & (BUFFER_SZ - 1));
				wasOverflow = 0;
			} else {
				if (wasOverflow == 0) {
					printk(KERN_ERR "%s: Event buffer overflow - IRQs will be missed", THIS_MODULE->name);
					wasOverflow = 1;
				}
			}

			interval_start = interval_end;
			interval_end = current_time;
			interval_value = value;
		}
	} else {
		// spurious interval
		// keep start, move end
		interval_end = current_time;
	}

	return IRQ_HANDLED;
}


static int rx433_open(struct inode *inode, struct file *file)
{
	int ret;

	// initialize irq time and queue management
	getnstimeofday(&interval_end);
	interval_start = interval_end;
	interval_value = 0;
	pHead = 0;
	pTail = 0;
	wasOverflow = 0;

	// register GPIO PIN in use
	ret = gpio_request(gpio_pin, "RX RF 433 OOK signal");
	if (ret) {
		printk(KERN_ERR "%s: Unable to request GPIOs for RX Signals: %d\n", THIS_MODULE->name, ret);
		return ret;
	}

	ret = gpio_direction_input(gpio_pin);
	if (ret) {
		printk(KERN_ERR "%s: Unable to set pin direction: %d\n", THIS_MODULE->name, ret);
		gpio_free(gpio_pin);
		return ret;
	}


	// Register IRQ for this GPIO
	ret = gpio_to_irq(gpio_pin);
	if (ret < 0) {
		printk(KERN_ERR "%s: Unable to request IRQ: %d\n", THIS_MODULE->name, ret);
		gpio_free(gpio_pin);
		return ret;
	}
	rx_irq = ret;
	ret = request_irq(rx_irq, rx_isr, IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING, IRQ_ENTRIES_NAME, NULL);
	if (ret) {
		printk(KERN_ERR "%s: Unable to request IRQ: %d\n", THIS_MODULE->name, ret);
		free_irq(rx_irq, NULL);
		gpio_free(gpio_pin);
		return ret;
	}
	
	printk(KERN_INFO "%s: Device opened (RX IRQ # %d)\n", THIS_MODULE->name, rx_irq);

	return 0;
}

static int rx433_release(struct inode *inode, struct file *file)
{
	free_irq(rx_irq, NULL);	
	gpio_free(gpio_pin);
	rx_irq = -1;
	printk(KERN_INFO "%s: Device closed\n", THIS_MODULE->name);
    return 0;
}

static ssize_t rx433_write(struct file *file, const char __user *buf, size_t count, loff_t *pos)
{
	return -EINVAL;
}

static ssize_t rx433_read(struct file *file, char __user *buf, size_t count, loff_t *pos)
{
	// returns one of the line with the time between two IRQs
	// return 0 : end of reading
	// return >0 : size
	// return -EFAULT : error
	char tmp[256];
	int _count;
	int _error_count;
	long item = 0;
	int head;
	int tail;

	/*spin_lock(&spinlock);
	head = pHead;
	tail = pTail;
	if (CIRC_CNT(head, tail, BUFFER_SZ) >= 1) {
		item = event_buff[tail];
		pTail = (tail + 1) & (BUFFER_SZ - 1);
	}
	spin_unlock(&spinlock);*/
	
	head = smp_load_acquire(&pHead);
	tail = pTail;
	if (CIRC_CNT(head, tail, BUFFER_SZ) > 0) {
		item = event_buff[tail];
		smp_store_release(&pTail, (tail + 1) & (BUFFER_SZ - 1));
	}

	_count = 0;
	if (item != 0) {
		if (item < 0) {
			sprintf(tmp, "0 %ld\n", -item);
		} else {
			sprintf(tmp, "1 %ld\n", item);
		}
  	    _count = strlen(tmp);
        _error_count = copy_to_user(buf, tmp, _count + 1);
        if (_error_count != 0) {
        	printk(KERN_ERR "%s: Error writing to char device", THIS_MODULE->name);
            return -EFAULT;
        }
	}

	return _count;
}

static struct file_operations rx433_fops = {
    .owner = THIS_MODULE,
    .open = rx433_open,
    .read = rx433_read,
    .write = rx433_write,
    .release = rx433_release,
};

static struct miscdevice rx433_misc_device = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = DEV_NAME,
    .fops = &rx433_fops,
	.mode = S_IRUSR | S_IRGRP,
};

/*
 * Module init function
 */
static int __init rpi_rf_init(void)
{
	if (gpio_pin < 1) {
		printk(KERN_ERR "%s: I need GPIO pin\n", THIS_MODULE->name);
		return -EINVAL;
	}
	
	printk(KERN_INFO "%s: Init using GPIO pin: %d\n", THIS_MODULE->name, gpio_pin);

	// Register a character device for communication with user space
    misc_register(&rx433_misc_device);

	return 0;
}

/**
 * Module exit function
 */
static void __exit rpi_rf_exit(void)
{
	printk(KERN_INFO "%s: Exit\n", THIS_MODULE->name);

    misc_deregister(&rx433_misc_device);
}

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Mc");
MODULE_DESCRIPTION("Linux Kernel Module for receiving RF OOK pulses");

module_init(rpi_rf_init);
module_exit(rpi_rf_exit);
