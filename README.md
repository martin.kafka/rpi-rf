RPI-RF
======

**rpi-rf** is sample application for receiving data from meteostation sensors 
communicating by Nexus protocol on 433 Mhz for Raspberry PI 2 and 3. Because 
it is using kernel module, it creates negligible CPU load and has much 
better time resolution of received pulses.

Kernel module is inspired by 
https://www.disk91.com/2015/technology/systems/rf433-raspberry-pi-gpio-kernel-driver-for-interrupt-management/
and
http://www.blaess.fr/christophe/2014/01/22/gpio-du-raspberry-pi-mesure-de-frequence/.

Usage
-----

Before compiling install raspberrypi-kernel-headers. Then run:
```
make modules
sudo insmod rpi_rf_ook.ko gpio_pin=N
python ./rpi_rf_receive.py
```

To remove module run:
```
sudo rmmod rpi_rf_ook
```

To install module into system:
```
sudo make modules_install
sudo depmod
```
then add `rpi_rf_ook` module to `/etc/modules`, into `/etc/modprobe.d/rpi_rf_ook.conf` add:
```
options rpi_rf_ook gpio_pin=17
```
then add `/etc/udev/rules.d/99-rpi_rf_ook.rules` with content:
```
KERNEL=="rpirfook", GROUP="gpio", MODE="0440", TAG+="systemd"
```
this will change permissions and ownership if new device and tag it for systemd.

To build and install module for kernel, which is not running (e.g. after upgrade) run:
```
make KERNEL_DIR=/lib/modules/<version>/build/ modules
sudo make KERNEL_DIR=/lib/modules/<version>/build/ modules_install
sudo depmod -a <version>
```
